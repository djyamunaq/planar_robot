#include "kinematic_model.h"

RobotModel::RobotModel(const double &l1In, const double &l2In): l1  (l1In), l2  (l2In) {

};

void RobotModel::FwdKin(Eigen::Vector2d &xOut, Eigen::Matrix2d &JOut, const Eigen::Vector2d & qIn){
  // TODO Implement the forward and differential kinematics
  double j11, j12, j21, j22;
  j11 = -this->l1*sin(qIn[0]) - this->l2*sin(qIn[0] + qIn[1]);
  j12 = -this->l2*sin(qIn[0] + qIn[1]); 
  j21 = this->l1*cos(qIn[0]) + this->l2*cos(qIn[0] + qIn[1]);
  j22 = this->l2*cos(qIn[0] + qIn[1]); 

  JOut << j11, j12, j21, j22;
  
  double x0 = this->l1*cos(qIn[0]) + this->l2*cos(qIn[0]+qIn[1]);
  double x1 = this->l1*sin(qIn[0]) + this->l2*sin(qIn[0]+qIn[1]);

  xOut << x0, x1;
}