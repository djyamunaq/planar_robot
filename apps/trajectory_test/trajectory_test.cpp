#include "trajectory_generation.h"
#include <iostream>

using std::cout;
using std::endl;

int main(){
  cout << "=== Trajectory Test ===" << endl;

  Polynomial pol(2, 7, 2, 3);
  Point2Point p2p(Eigen::Vector2d(1, 2), Eigen::Vector2d(5, 8), 3, 5);

  // Compute the trajectory for given initial and final positions. 
  // Display some of the computed points
  cout << "Trajectory: " << endl;
  double dt = 0.001; 
  for(double t=5; t<=8; t+=dt) {
    cout << p2p.X(t)(0) << ", " << p2p.X(t)(1) << endl;
  }
  // Check numerically the velocities given by the library 
  // Check initial and final velocities
  cout << "Velocity: " << endl;
  for(double t=5; t<=8; t+=dt) {
    float dXa = p2p.dX(t)(0);
    float dYa = p2p.dX(t)(1);

    float dXn = (p2p.X(t+dt)(0) - p2p.X(t)(0))/dt;
    float dYn = (p2p.X(t+dt)(1) - p2p.X(t)(1))/dt;

    cout << "N: " << dXa << ", " << dYa << endl;
    cout << "A: " << dXn << ", " << dYn << endl << endl;
  }

  cout << "=======================" << endl;
}