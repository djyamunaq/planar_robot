#include "kinematic_model.h"
#include <iostream>

using std::cout;
using std::endl;

int main(){
  cout << "=== Kinematic Model ===" << endl;
  
  RobotModel rm(1, 1);
  
  // Compute the forward kinematics and jacobian matrix for 
  //      q =  M_PI/3.0,  M_PI_4
  Eigen::Vector2d qIn, xOut, xPrev;
  Eigen::Matrix2d JOut;
  qIn << M_PI/3.0, 0;
  
  rm.FwdKin(xOut, JOut, qIn);
  cout << "X(q) = " << xOut(0) << ", " << xOut(1) << endl; 

  // For a small variation of q, compute the variation on X and check dx = J . dq  
  xPrev = xOut;
  Eigen::Vector2d dq = 0.01*qIn; 
  rm.FwdKin(xOut, JOut, qIn+dq);
  cout << "X(q + dq) = " << xOut(0) << ", " << xOut(1) << endl;

  cout << "dx = " << (xOut - xPrev).transpose() << endl;
  cout << "J.dq = " << (JOut*dq).transpose() << endl;
  

  cout << "=======================" << endl;
}
