#include "control.h"
#include "trajectory_generation.h"
#include <iostream>

using std::cout;
using std::endl;

int main(){
  cout << "=== Control Test ===" << endl;

  // Show using all three libraries, simulate the motion of a planar robot
  // For a initial position 
  //      q   = M_PI_2, M_PI_4
  
  Eigen::Vector2d qIn, x, xPrev;
  Eigen::Matrix2d JOut;
  qIn << M_PI/3.0, M_PI_4;
  
  // and  
  //      l1  = 0.4, l2 = 0.5
  
  RobotModel rm(0.4, 0.5);
  rm.FwdKin(x, JOut, qIn);

  Controller c(rm);
  
  // simulate a motion achieving 
  //      Xf  = 0.0, 0.6
  
  Eigen::Vector2d xd, Dxd_ff;
  xd << 0.0, 0.6;
  const double tf = 2;
  Point2Point p2p(x, xd, tf, 0);

  double dt = 1e-3;
  for(double t=0; t<tf; t+=dt) {
    Eigen::Vector2d Dxd_ff = p2p.dX(t);
    Eigen::Vector2d x = p2p.X(t);
    Eigen::Vector2d xd = p2p.X(t+dt);
    Eigen::Vector2d dq = c.Dqd(qIn, xd, Dxd_ff); 

    cout << "t=" << t << endl;
    cout << "x p2p=" << x.transpose() << endl;
    rm.FwdKin(x, JOut, qIn);
    cout << "x rm=" << x.transpose() << endl; 

    cout << "xd=" << xd.transpose() << endl;
    cout << "dx=" << Dxd_ff.transpose() << endl;
    cout << "q=" << qIn.transpose() << endl;
    cout << "dq=" << dq.transpose() << endl;
    cout << endl;
    
    qIn = qIn + dq*dt;
  }
  
  cout << "=======================" << endl;
}